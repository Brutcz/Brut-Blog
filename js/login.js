$(document).ready(function(){
	$('#logRegTabList a').click(function(e){
		e.preventDefault();
		var href = $(this).attr('href');
		var old_href = $('#logRegTabList li.active a').attr('href');

		$('#logRegTabList li.active').removeClass("active");
		$(this).parent().addClass("active");

		$(old_href).removeClass("in active");
		$(href).addClass("in active");
	});

	$('.tab-content input').focus(function(){
		$(this).popover('show');
	});

	$('.tab-content input').blur(function(){
		$(this).popover('hide');
	});
	$('#registerForm input[name=name]').keyup(function(){
		var formInput=$(this);
		var username=formInput.val();
		var divInput=formInput.parent();
		if(username.length>0){
			var isAvailable = checkName(username);
			if(isAvailable){
				divInput.removeClass("has-error");
				divInput.addClass("has-success");
			} else {
				divInput.removeClass("has-success");
				divInput.addClass("has-error");
			}

		} else{
			divInput.removeClass("has-error");
			divInput.removeClass("has-success");
		} 
	});
});


function checkName(name){
	var ajaxData = "name="+name;
	var is;
        $.ajax({
            url: "./tools/checkName.php",
            type: "POST",
            async: false,
            data: ajaxData,
            dataType: "json"
        }).done(function(data){

            if(data[0]==true){
            	is=true;
            }
            else {
            	is=false;
            }
        });

    return is;
}