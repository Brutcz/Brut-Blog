$(document).ready(function(){
	$('.delete_article').hover(function(){
		$(this).removeClass('fa-times-circle-o').addClass('fa-times-circle');
	},function(){
		$(this).removeClass('fa-times-circle').addClass('fa-times-circle-o');
	});

	$('.delete_article').click(function(){
		var id = $(this).parent().parent().attr('id');
		if(confirm("Opravdu chcete smazat tento příspěvek? ("+id+")")){
			//console.log("OK");
			deleteArticle(id);
		}
	});

    $('.edit_article').click(function(){
        var id = $(this).parent().parent().attr('id');
        var title = $('#'+id).find('.panel-heading h3').text().trim();
        //var text = $(this).parent().parent().parent().find('.panel-body p').text().trim();
        
        if(id!=undefined){
            if(confirm("Opravdu chcete editovat tento příspěvek? ("+id+")")){
                //console.log("OK");
                $('#editModal').modal('show');
                $('#myModalLabel').html('Editace příspěvku #'+id);
                //$('#postNews').html(text);
                var text = getArticle(id);
                $('#postNews_ifr').contents().find('p').html(text);
                $('#newsEditForm input[name="title"]').val(title);
                $('#newsEditForm #id_article').val(id);
            }
        } else window.alert("Nemohli jsme najít id článku");

    });

});

function deleteArticle(id){

	var ajaxData = "id="+id;
        $.ajax({
            url: "./tools/deleteArticle.php",
            type: "POST",
            data: ajaxData,
            dataType: "json"
        }).done(function(data){
            //console.log(data); //ověření dat

            if(data[0]==true){
                console.log("succes"); //pouze kontrola funkčnosti
                $('#'+id).fadeOut("slow");
            }
            else {
                console.log("fail"); //pouze kontrola funkčnosti
                window.alert("Článek se nepodařilo odstranit!");

            }
        });
}

function getArticle(id){

    var text="";
    var ajaxData = "id="+id;
        $.ajax({
            url: "./tools/getArticleText.php",
            type: "POST",
            async:false,
            data: ajaxData,
            dataType: "json"
        }).done(function(data){
            //console.log(data); //ověření dat

            if(data[0]==true){
                console.log("succes"); //pouze kontrola funkčnosti
                text=data[1];
                //console.log(text);    //kontrola výpisu
            }
            else {
                console.log("fail"); //pouze kontrola funkčnosti
            }
        });

        return text;
}

function updateArticle(){

    var post = $('#postNews_ifr').contents().find('p').html();
    var url = $('#newsEditForm').serialize();
    var id =$('#newsEditForm').serializeArray()[1].value;
    var ajaxData = url+post;
        $.ajax({
            url: "./tools/updateArticle.php",
            type: "POST",
            data: ajaxData,
            dataType: "json"
        }).done(function(data){
            //console.log(data); //ověření dat

            if(data[0]==true){
                console.log("succes"); //pouze kontrola funkčnosti

                var html = "<p>"+getArticle(id)+"</p>";
                $('#'+id+' .panel-body p').html(html);
                $('#editModal').modal('hide');
            }
            else {
                console.log("fail"); //pouze kontrola funkčnosti

            }
        });
}