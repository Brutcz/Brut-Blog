$(document).ready(function(){
	$('.delete_comment').click(function(){
		var id = $(this).parent().parent().attr('id');
		deleteComment(id);
	});
	$('#loadMoreComment').click(function(){
		var id= $('.panel:last').attr('id');
        $(this).button('loading');
		loadMoreComment(id);
	});
});

function writeComment(id_article){
	$('#commentBtn').button('loading');
	var comment=$('#commentText').val();
	var form = $('#commentForm').serialize().replace("%40","@");
	var ajaxData = "id="+id_article+"&comment="+comment+"&"+form;
        $.ajax({
            url: "./tools/writeComment.php",
            type: "POST",
            data: ajaxData,
            dataType: "json"
        }).done(function(data){
            //console.log(data); //ověření dat
            if(data[0]==true){
                //console.log("succes"); //pouze kontrola funkčnosti
                $('h2').after(data[1]);
                $('#commentBtn').button('reset');
            }
            else {
                console.log("fail"); //pouze kontrola funkčnosti
            }
        });
}

function deleteComment(id){
	var ajaxData = "id="+id+"&idArticle="+article;
        $.ajax({
            url: "./tools/deleteComment.php",
            type: "POST",
            data: ajaxData,
            dataType: "json"
        }).done(function(data){
            //console.log(data); //ověření dat
            if(data[0]==true){
                //console.log("succes"); //pouze kontrola funkčnosti
                $('#'+id).fadeOut("slow");
            }
            else {
                console.log("fail"); //pouze kontrola funkčnosti
            }
        });
}

function loadMoreComment(id){
	var ajaxData = "id="+id+"&idArticle="+article;
        $.ajax({
            url: "./tools/getMoreComment.php",
            type: "POST",
            data: ajaxData,
            dataType: "json"
        }).done(function(data){
            //console.log(data); //ověření dat
            if(data[0]==true){
                //console.log("succes"); //pouze kontrola funkčnosti
                for(var i=1;i<data.length;i++)
                {
                	$('#'+id).after(data[i]);
                }
                $('#loadMoreComment').button('reset');
            }
            else {
                console.log("fail"); //pouze kontrola funkčnosti
                window.alert("Komentáře se nepodařilo načíst nebo už jsou všechny načteny");
            }
        });
}