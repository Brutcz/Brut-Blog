<?php
	include('db_conn.php');

	$username = $_POST['name']; 
	$password = md5($_POST['password']);
	$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
	$stmt=$mysqli->prepare("SELECT `Activated`,`id` FROM `members` WHERE `username` =? AND `password` =?");
	$stmt->bind_param("ss",$username,$password);
	$stmt->execute();
	$stmt->store_result();
	$n = $stmt->num_rows;
	if($n==1)
	{
		$stmt->bind_result($activated,$userID);
		$stmt->fetch();
		$stmt->close();
		$random = md5(date("%Y-%n-%d-%U"));
		$_SESSION['login']['nick'] = $username;
		$_SESSION['login']['ID'] = $userID;
		$_SESSION['login']['actived'] = $activated;
		$_SESSION['login']['sessionCode'] = $random;
		$stmt=$mysqli->prepare("REPLACE INTO `uaccess` VALUES(?,?,INET_ATON(?))");
		$stmt->bind_param("iss",$userID,$random,$ip);
		$stmt->execute();
		$stmt->close();
		header("Location: ./");
	}
	else 
	{
		$stmt->close();
		$_SESSION['msg']['error_type'][1]="Špatné jméno nebo heslo";
		$_SESSION['msg']['error_type'][0]=1;
		header("Location: ./prihlaseni");
	}
	
?>