<!DOCTYPE html>
<html>
  <head>
    <title>Blogísek - Přihlášení/Registrace</title>
    <?php include('head.php'); ?>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron" style="min-height: 500px;">
  				<div class="center" style="width:50%;">
					<ul class="nav nav-tabs" role="tablist" id="logRegTabList">
						<li class="active"><a href="#register">Registrace</a></li>
						<li><a href="#login">Přihlášení</a></li>
					</ul>

					<script src="./js/login.js"></script>
					<div class="tab-content">
						<div class="tab-pane fade in active" id="register">
							<form method="POST" id="registerForm" action="register.php" style="margin-top:15px;"> 
								<fieldset>
			                        <div class="input-group">
			                          	<span class="input-group-addon"><i class="fa fa-user"></i></span>
			                          	<input name="name" type="text" class="form-control" placeholder="Uživatelské jméno" rel="popover" data-container="body" data-placement="right" data-content="Napiš své přihlašovací jméno">
			                        </div>
			                        <div class="input-group">
			                          	<span class="input-group-addon"><i class="fa fa-at"></i></span>
			                          	<input name="email" type="text" class="form-control" placeholder="Email" rel="popover" data-container="body" data-placement="right" data-content="Napiš svůj email">
			                        </div>
			                        <div class="input-group">
			                          	<span class="input-group-addon"><i class="fa fa-lock"></i> </span>
			                          	<input name="password" type="password" class="form-control" placeholder="Heslo" rel="popover" data-container="body" data-placement="right" data-content="Napiš své heslo">
			                        </div> 
			                        <div class="input-group">
			                          	<span class="input-group-addon"><i class="fa fa-lock"></i> </span>
			                          	<input name="password_again" type="password" class="form-control" placeholder="Heslo znovu" rel="popover" data-container="body" data-placement="right" data-content="Napiš své heslo znovu">
			                        </div>
			                        <div class="btn-group" data-toggle="buttons" style="margin-bottom:5px;margin-top:10px;">
										<label for="sex_f" class="btn btn-primary active"><input type="radio" name="sex" value="F" id="sex_f" checked><i class="fa fa-venus"></i>&nbsp; Žena &nbsp;</label>
										<label for="sex_m" class="btn btn-primary"><input type="radio" name="sex" value="M" id="sex_m"><i class="fa fa-mars"></i>&nbsp; Muž &nbsp;</label>
									</div>
									<div class="checkbox">
							          	<label>
							            	<input type="checkbox" name="terms" id="terms" value="1" required>
							            	Souhlasím s <a href="terms_of_use.php" target="_blank">podmínkami</a> použití
							          	</label>
							        </div>
			                        <p><button id="registerBtn" class="btn btn-primary" style="margin-top:10px;">Registrovat</button></p>
		                        </fieldset>
	                      </form>
						</div>
						<div class="tab-pane fade" id="login">
							<form method="POST" id="loginForm" action="./login.php" style="margin-top:15px;">
								<fieldset>
			                        <div class="input-group">
			                          	<span class="input-group-addon"><i class="fa fa-user"></i></span>
			                          	<input name="name" type="text" class="form-control" placeholder="Uživatelské jméno" rel="popover" data-container="body" data-placement="right" data-content="Napiš své přihlašovací jméno">
			                        </div>
			                        <div class="input-group">
			                          	<span class="input-group-addon"><i class="fa fa-lock"></i></span>
			                        	<input name="password" type="password" class="form-control" placeholder="Heslo" rel="popover" data-container="body" data-placement="right" data-content="Napiš své heslo">
			                        </div>
			                        <p style="margin-top: 10px;margin-bottom: 0;font-size: 10pt;">Zapomenuté heslo? Nevadí, stačí kliknout <a href="./zapomenuteHeslo">zde</a>!</p>
			                        <p><button type="submit" id="loginBtn" class="btn btn-primary" style="margin-top:10px;">Přihlásit se</button></p>
		                        </fieldset>    
	                      </form>
						</div>
					</div>
				</div>
  			</div>
  		</div>
  	</div>
  </body>
</html>