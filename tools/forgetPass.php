<?php
	include('../db_conn.php');

	$name=$_POST['name'];
	$email=$_POST['email'];

	$stmt=$mysqli->prepare("SELECT id FROM members WHERE usename=? AND email=?");
	$stmt->bind_param("ss",$name,$email);
	$stmt->execute();
	$stmt->bind_result($id);
	$stmt->store_resutl();
	$n=$stmt->num_rows;
	if($n==1)
	{
		$stmt->close();

		$id=getUserID($name);

		$stmt=$mysqli->prepare("SELECT * FROM user_password_change WHERE id_user=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$stmt->store_result();
		$n=$stmt->num_rows;
		$stmt->close();
		if($n==1)
		{
			$_SESSION['msg']['error_type'][1]="Už máte jeden token vytvořený";
			$_SESSION['msg']['error_type'][0]=1;
			header("Location: ./");
		} else {
			
			$token=md5(uniqid(rand(), true));

			$stmt=$mysqli->prepare("INSERT INTO user_password_change (id_user, pass_token, pass_token_validity) VALUES(?,?,NOW()+INTERVAL 1 DAY)");
			$stmt->bind_param("iss",$id,md5($token));
			$stmt->execute();
			$stmt->close();

			mail($email, "Zapomenute heslo", "http://".$_SERVER[SERVER_NAME]."/tools/passRecovery.php?login=".urlencode($name)."&token=".$token."\n\n\n V přípagě, že jsete si heslo nevyžádali, tak prosím tento email ignorujte.");

			$_SESSION['msg']['error_type'][1]="Další instrukce byly odeslány na váš email";
			$_SESSION['msg']['error_type'][0]=2;
			header("Location: ./");
		}

		
	} else {
		$stmt->close();
		$_SESSION['msg']['error_type'][1]="Špatné jméno nebo email";
		$_SESSION['msg']['error_type'][0]=1;
		header("Location: ./");
	}

?>