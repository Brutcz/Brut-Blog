<?php
	include('../db_conn.php');

	$name=$_POST['name'];
	$id=$_POST['id'];
	$token=$_POST['token'];
	$pass=md5($_POST['password']);
	$pass_again=md5($_POST['password_again']);

	if(strcmp($pass,$pass_again)==0)
	{
		$stmt=$mysqli->prepare("UPDATE members SET password=? WHERE id=?");
		$stmt->bind_param("si",$pass,$id);
		$stmt->execute();
		$stmt->close();

		$stmt=$mysqli->prepare("DELETE FROM user_password_change WHERE id_user=? AND pass_token=?");
		$stmt->bind_param("is",$id,md5($token));
		$stmt->execute();
		$stmt->close();
	} else {
		$_SESSION['msg']['error_type'][1]="Hesla se neshodují";
		$_SESSION['msg']['error_type'][0]=1;
		header("Location: ./tools/passRecovery.php?login=".$name."&token=".$token);
	}
?>