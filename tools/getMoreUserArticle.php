<?php
	include('../db_conn.php');

	$id=$_POST['id'];
	$UID=$_POST['UID'];
	$data=array();
	$data[0]=true;

	$stmt=$mysqli->prepare(
	"SELECT A.id_article, A.title, C.name, DATE(date) AS date, A.for_registred 
	FROM article A 
	LEFT JOIN category C ON (C.id_category=A.category)
	WHERE A.author=? AND A.id_article<? 
	ORDER BY A.id_article ASC 
	LIMIT 5");
	$stmt->bind_param("ii",$UID,$id);
	$stmt->execute();
	$stmt->bind_result($id_article,$title,$category,$date,$for_only);
	$stmt->store_result();
	$n = $stmt->num_rows;
	if($n<1) $data[0]=false;
	while($stmt->fetch())
	{
		$class=(($for_only==1)&&(!isset($_SESSION['login'])))?"list-group-item disabled":"list-group-item";
		array_push($data, '<a id="'.$id_article.'" class="'.$class.'" href="./clanek/'.$id_article.'">'.$title.'&nbsp;&nbsp;<span class="label label-warning">'.$category.'</span>&nbsp;&nbsp;'.$date.'</a>');
	}
	$stmt->close();

	echo json_encode($data);
?>