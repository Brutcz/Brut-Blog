<?php
	include('../db_conn.php');

	$id=$_POST['id'];
	$id_article=$_POST['idArticle'];

	$data=array();

	$data[0]=true;

	$stmt=$mysqli->prepare("DELETE FROM comment WHERE id_comment=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$stmt->close();

	$stmt=$mysqli->prepare("UPDATE article SET comment_count=comment_count-1 WHERE id_article=?");
	$stmt->bind_param("i",$id_article);
	$stmt->execute();
	$stmt->close();

	echo json_encode($data);
?>