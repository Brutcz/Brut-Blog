<?php
	include('../db_conn.php');

	header('Content-Type: application/json ; charset=utf-8');

	$id = $_POST['id'];
	$data = array();

	$data[0]=false;

	if(isUserLogIn($_SESSION['login']['ID'],$_SESSION['login']['sessionCode']))
	{
		if(haveUserPerm($_SESSION['login']['nick'],'article_change'))
		{

			$stmt=$mysqli->prepare("DELETE FROM article WHERE id_article=?");
			$stmt->bind_param("i",$id);
			$stmt->execute();
			$stmt->close();

			$data[0]=true;
		}
	}

	echo json_encode($data);

?>