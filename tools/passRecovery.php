<!DOCTYPE html>
<html>
  <head>
    <title>Brut Blog - zvolení nového hesla</title>
    <?php include('../head.php'); ?>
  </head>
  <body>
	<?php include('../nav.php'); ?>  	
	<?php
		if(!isset($_GET['login']) || !isset($_GET['token']))
		{
			$_SESSION['msg']['error_type'][1]="Neplatné parametry";
			$_SESSION['msg']['error_type'][0]=1;
			header("Location: /");
		}
		
		$name=$_GET['login'];
		$id=getUserID($name);
		$token=$_GET['token'];


		$stmt=$mysqli->prepare("SELECT * FROM user_password_change WHERE id_user=? AND pass_token=? AND pass_token_validity>=NOW()");
		$stmt->bind_param("is",$id,md5($token));
		$stmt->execute();
		$stmt->store_result();
		$n=$stmt->num_rows;
		$stmt->close();
		if($n==0)
		{
			$stmt=$mysqli->prepare("DELETE FROM user_password_change WHERE id_user=? AND pass_token=?");
			$stmt->bind_param("is",$id,md5($token));
			$stmt->execute();
			$stmt->close();

			$_SESSION['msg']['error_type'][1]="Tento token už není platný";
			$_SESSION['msg']['error_type'][0]=1;
			header("Location: /");
		}

	?>
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1>Zvolení nového hesla</h1>
			    <form method="post" action="./tools/setNewPass.php" class="center-block" style="width:50%;">
			    	<fieldset>
			    		<div class="input-group">
			              	<span class="input-group-addon"><i class="fa fa-lock"></i> </span>
			                <input name="password" type="password" class="form-control" placeholder="Heslo" rel="popover" data-container="body" data-placement="right" data-content="Napiš své heslo">
			            </div> 
			            <div class="input-group">
			                <span class="input-group-addon"><i class="fa fa-lock"></i> </span>
			                <input name="password_again" type="password" class="form-control" placeholder="Heslo znovu" rel="popover" data-container="body" data-placement="right" data-content="Napiš své heslo znovu">
			            </div>
			            <input type="hidden" name="name" value="<?php echo $name;?>">
			            <input type="hidden" name="id" value="<?php echo $id;?>">
			            <input type="hidden" name="token" value="<?php echo $token;?>">
			            <p><button id="getPass" class="btn btn-primary center-block" style="margin-top:10px;">Zvolit nové heslo</button></p>
			    	</fieldset>
			    </form>
  			</div>
  		</div>
  	</div>
  </body>
</html>
