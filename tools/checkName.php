<?php
	include('../db_conn.php');

	$name=$_POST['name'];
	$data=array();

	$stmt=$mysqli->prepare("SELECT id FROM members WHERE username=?");
	$stmt->bind_param("s",$name);
	$stmt->execute();
	$stmt->store_result();
	$n=$stmt->num_rows;
	$stmt->close();

	$data[0]=($n==0)? true:false;
		
	echo json_encode($data);
?>