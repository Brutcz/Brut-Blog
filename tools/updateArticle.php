<?php
	include('../db_conn.php');

	$id=$_POST['id_article'];
	$title=htmlentities($_POST['title']);
	$onlyFor=!empty($_POST['onlyFor'])? $_POST['onlyFor']:0;
	$post=htmlentities($_POST['post']);//mysql_escape_String(htmlentities($_POST['post']));
	$data=array();

	$data[0]=true;

	$stmt=$mysqli->prepare("UPDATE article SET title=?, article=?,for_registred=? WHERE id_article=?");
	$stmt->bind_param("ssdd",$title,$post,$onlyFor,$id);
	$stmt->execute();
	$stmt->close();

	echo json_encode($data);
?>