<?php
	include('../db_conn.php');

	$id=$_POST['id'];
	$comment=htmlentities($_POST['comment']);
	$username=htmlentities($_POST['name']);
	$email=htmlentities($_POST['email']);
	$ip = $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
	$data=array();

	$data[0]=true;

	$stmt=$mysqli->prepare("INSERT INTO comment(comment,author,date,IP,article,email) VALUES(?,?,NOW(),INET_ATON(?),?,?)");
	$stmt->bind_param("sssis",$comment,$username,$ip,$id,$email);
	$stmt->execute();
	$comment_id=$stmt->insert_id;
	$stmt->close();

	$stmt=$mysqli->prepare("UPDATE article SET comment_count=comment_count+1 WHERE id_article=?");
	$stmt->bind_param("i",$id);
	$stmt->execute();
	$stmt->close();

	//$stmt=mysqli->prepare("SELECT id_comment FROM comment WHERE ");

	$username=html_entity_decode($username);
	$delete = (strcmp($username,$_SESSION['login']['nick'])==0)?'<i class="fa fa-2x fa-times-circle-o delete_comment"></i>':'';
	$date=date("Y-m-d");
	$comment=html_entity_decode($comment);

	array_push($data, '<div id="'.$comment_id.'" class="panel panel-primary"><div class="panel-heading">Komentář napsal/a '.$username.' v '.$date.$delete.'</div><div class="panel-body">'.$comment.'</div></div>');

	echo json_encode($data);
?>