<?php
	include('../db_conn.php');

	$id=$_POST['id'];
	$data=array();
	$data[0]=true;

	if(haveUserPerm($_SESSION['login']['nick'],'article_change'))
	{

		$stmt=$mysqli->prepare("SELECT article FROM article WHERE id_article=?");
		$stmt->bind_param("i",$id);
		$stmt->execute();
		$stmt->bind_result($article);
		$stmt->fetch();
		$stmt->close();
		
		$article=getTextBetweenTags(html_entity_decode($article),"p");

		array_push($data, $article);

	} else $data[0]=false;

	echo json_encode($data);
?>