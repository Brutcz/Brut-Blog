<?php
	include('../db_conn.php');

	$title=htmlentities($_POST['title']);
	$category=$_POST['articleCategory'];
	$onlyFor=!empty($_POST['onlyFor'])? $_POST['onlyFor']:0;
	$post=htmlentities($_POST['post']);	//mysql_escape_String(htmlentities($_POST['post']));
	$type=$_POST['type'];

	$stmt=$mysqli->prepare("INSERT INTO `article`(`id_article`, `title`, `article`, `category`, `author`, `date`, `for_registred`,`type`) VALUES ('',?,?,?,?,NOW(),?,?)");
	$stmt->bind_param("ssddds",$title,$post,$category,$_SESSION['login']['ID'],$onlyFor,$type);
	$stmt->execute();
	$stmt->close();

	$_SESSION['msg']['error_type'][1]="Článek byl přidán";
	$_SESSION['msg']['error_type'][0]=2;
	header("Location: /");
?>