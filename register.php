<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
</head>
<body>
<?php
	include ('db_conn.php');

	$username=$_POST['name'];
	$email=$_POST['email'];
	$password=md5($_POST['password']);
	$password_again=md5($_POST['password_again']);
	$sex=$_POST['sex'];

	$confirm_code=md5(uniqid(rand()));

	if(filter_var($email,FILTER_VALIDATE_EMAIL)){

		if(strcmp($password,$password_again)==0){

			$stmt=$mysqli->prepare("SELECT `username` FROM `members` WHERE `username`=?");
			$stmt->bind_param("s",$username);
			$stmt->execute();
			$stmt->store_result();
			$numRow=$stmt->num_rows;
			$stmt->close();

			if($numRow < 1){

				$stmt=$mysqli->prepare("INSERT INTO `members`(`id`,`username`, `password`, `email`, `sex`, `regDate`, `Activated`) VALUES ('',?,?,?,?,NOW(),'0')");
				$stmt->bind_param("ssss",$username,$password,$email,$sex);
				$stmt->execute();
				$stmt->close();

				$stmt=$mysqli->prepare("SELECT `id` FROM `members` WHERE `username` =? AND `password` =?");
				$stmt->bind_param("ss",$username,$password);
				$stmt->execute();
				$stmt->bind_result($userID);
				$stmt->fetch();
				$stmt->close();

				$stmt=$mysqli->prepare("INSERT INTO `users_confirm_codes` VALUES(?,?)");
				$stmt->bind_param("ds",$userID,$confirm_code);
				$stmt->execute();
				$stmt->close();

				$stmt=$mysqli->prepare("INSERT INTO `user_role`(`id_user_role`, `id_user`, `id_role`) VALUES ('',?,'1')");
				$stmt->bind_param("d",$userID);
				$stmt->execute();
				$stmt->close();

				$_SESSION['msg']['error_type'][1]="Registrace byla úspěšná";
				$_SESSION['msg']['error_type'][0]=2;
				header("location: ./prihlaseni");
			}
			else{ 
				$_SESSION['msg']['error_type'][1]="Uživatelské jméno je již použité";
				$_SESSION['msg']['error_type'][0]=1;
				header("location: ./prihlaseni");
			}
		}
		else{ 
			$_SESSION['msg']['error_type'][1]="Neshodujíci se hesla";
			$_SESSION['msg']['error_type'][0]=1;
			header("location: ./prihlaseni");
		}
	}
	else{
		$_SESSION['msg']['error_type'][1]="Špatný tvar emailu";
		$_SESSION['msg']['error_type'][0]=1;
		header("location: ./prihlaseni");
	}
?>
</body>
</html>