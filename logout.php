<?php
	include('db_conn.php');
	
	$stmt=$mysqli->prepare("DELETE FROM `uaccess` WHERE `userID` =? AND `randomCode` =?");
	$stmt->bind_param("is",$_SESSION['login']['ID'],$_SESSION['login']['sessionCode']);
	$stmt->execute();
	$stmt->close();

	$mysqli->close();

	session_destroy();
	header("Location: ./");
?>