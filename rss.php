<rss version="2.0">
	<chanel>
		<title>Brut Blog</title>
		<link>http://88.81.76.204/</link>
		<description>Blog na kterém najdete programování a články</description>
	</chanel>
<?php
	header("Content-Type: application/xml; charset=utf-8"); 
	include('db_conn.php');

	$stmt=$mysqli->prepare(
	"SELECT A.id_article,A.title, A.article, C.name, DATE(date) AS date, M.username 
	FROM article A 
	LEFT JOIN category C ON (C.id_category=A.category)
	LEFT JOIN members M ON(M.id=A.author) 
	WHERE for_registred=0 AND type='article'
	LIMIT 20
	");
	$stmt->execute();
	$stmt->bind_result($id_article,$title,$article,$category,$date,$author);
	while($stmt->fetch())
	{
		$article=str_replace("<br>","<br/>",html_entity_decode($article));
		if(strlen($article)>70) $article="<p>".substr(trim(getTextBetweenTags($article,"p")),0,70)."...</p>";
			else $article="<p>".trim(getTextBetweenTags($article,"p"))."</p>";

		$id_article="http://88.81.76.204/clanek/".$id_article;
		?>
		<item>
			<title><?php echo $title;?></title>
			<link><?php echo $id_article;?></link>
			<description><?php echo $article;?></description>
			<date><?php echo $date;?></date>
			<author><?php echo $author;?></author>
		</item>
		<?php
	}
	$stmt->close();
?>
</rss>