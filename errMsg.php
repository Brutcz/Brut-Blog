<?php
if(isset($_SESSION['msg']['error_type'][1])){
		if($_SESSION['msg']['error_type'][0]==1){
		?>
			<!--Danger-->
			<div class="alert alert-danger alert-dismissible center" role="alert" style="width:50%;margin-top:85px;">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<strong>Pozor!</strong> <?php echo $_SESSION['msg']['error_type'][1]; ?>
			</div>
		<?php
		} elseif ($_SESSION['msg']['error_type'][0]==2){
		?>
			<!--Success-->
			<div class="alert alert-success alert-dismissible center" role="alert" style="width:50%;margin-top:85px;">
  				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  				<strong>Pozor!</strong> <?php echo $_SESSION['msg']['error_type'][1]; ?>
			</div>
		<?php
		}

		unset($_SESSION['msg']['error_type'][1]);
		unset($_SESSION['msg']['error_type'][0]);
	}
?>