<html>
  <head>
    <?php include('head.php'); ?>
<?php
	$article_id=trim($_GET['id']);

	$stmt=$mysqli->prepare(
	"SELECT A.title, A.article, C.name, M.username, DATE(date) AS date, A.for_registred, A.comment_count 
	FROM article A 
	LEFT JOIN category C ON (C.id_category=A.category) 
	LEFT JOIN members M ON(M.id=A.author) WHERE A.id_article=?");
	$stmt->bind_param("d",$article_id);
	$stmt->execute();
	$stmt->bind_result($title,$article,$category,$author,$date,$onlyFor,$comment_count);
	$stmt->fetch();
	$stmt->close();

	$article=html_entity_decode($article);
	$title=html_entity_decode($title);
?>
  <title>Brut Blog - <?php echo $title; ?></title>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1><?php echo $title; ?></h1>
			   	<hr>
			   	<?php if(($onlyFor==1)&&(isUserLogIn($_SESSION['login']['ID'],$_SESSION['login']['sessionCode'])))
				    		{
				    			echo $article;
				    		} elseif(($onlyFor==1)&&(!isUserLogIn($_SESSION['login']['ID'],$_SESSION['login']['sessionCode']))){
				    				echo '<div class="alert alert-info" role="alert">Pro čtení tohoto článku musíte být přihlášeni!</div>';
				    		} else {
				    			echo $article;
				    		}
				    	?>
			   	<hr>
			   	<p><?php echo 'Datum: '.$date.'<br>Autor: <a href="./uzivatel/'.$author.'">'.$author.'</a><br>Počet komentářů: '.$comment_count.'<br>'; ?><a href="#categorySelect"><span class="label label-warning"><?php echo $category;?></span></a></p>
  				<hr>
  				<?php
  					if(isset($_SESSION['login'])&&isUserLogIn($_SESSION['login']['ID'],$_SESSION['login']['sessionCode']))
  					{
  						$stmt=$mysqli->prepare("SELECT email,username FROM members WHERE id=?");
  						$stmt->bind_param("i",$_SESSION['login']['ID']);
  						$stmt->execute();
  						$stmt->bind_result($email,$username);
  						$stmt->fetch();
  						$stmt->close();
  						?>
  							<script>
  							$(document).ready(function(){
  								$('input[name=email]').val("<?php echo $email;?>");
  								$('input[name=name]').val("<?php echo $username;?>");
  							});
  							</script>
  						<?php
  					}
  				?>
  				<script src="./js/comment.js"></script>
  				<form id="commentForm" class="center" action="" method="POST" onsubmit="writeComment(<?php echo $article_id;?>); return false;" style="width:50%;">
  					<fieldset>
  						<legend>Napiš komentář</legend>
	  					<div class="input-group">
				          	<span class="input-group-addon"><i class="fa fa-user"></i></span>
				         	<input name="name" type="text" class="form-control" placeholder="Uživatelské jméno" rel="popover" data-container="body" data-placement="right" data-content="Napiš své přihlašovací jméno">
				        </div>
				        <div class="input-group">
				          	<span class="input-group-addon"><i class="fa fa-at"></i></span>
				         	<input name="email" type="email" class="form-control" placeholder="Email" rel="popover" data-container="body" data-placement="right" data-content="Napiš svůj email">
				        </div>
				        <textarea id="commentText" placeholder="Komentář...."></textarea>
				        <p><button id="commentBtn" class="btn btn-primary" type="submit" data-loading-text="Píšu komentář..." style="float:right;margin-top:10px;margin-bottom:0;">Napsat komentář</button></p>
			        </fieldset>
  				</form>
  				<h2>Komentáře</h2>
  				<?php
  					if($comment_count>0)
  					{
  						$stmt=$mysqli->prepare(
  						"SELECT C.id_comment, C.comment,C.author,DATE(C.date) AS date
  						FROM comment C
  						WHERE C.article=?
  						ORDER BY C.id_comment DESC
  						LIMIT 20
  						");
  						$stmt->bind_param("i",$article_id);
  						$stmt->execute();
  						$stmt->bind_result($comment_id,$comment,$username,$date);
  						while($stmt->fetch())
  						{
  							$comment=html_entity_decode($comment);
  							?>
  								<div id="<?php echo $comment_id;?>" class="panel panel-primary">
  									<div class="panel-heading">Komentář napsal/a <?php echo $username." v ".$date; if(isset($_SESSION['login'])&&strcmp($username,$_SESSION['login']['nick'])==0) echo '<i class="fa fa-2x fa-times-circle-o delete_comment"></i>';?></div>
  									<div class="panel-body">
  										<?php echo $comment;?>
  									</div>
  								</div>
  							<?php
  						}
  						$stmt->close();

  						if($comment_count>20) echo '<button id="loadMoreComment" class="btn btn-default center-block" data-loading-text="Načítám..." style="margin-top:15px;">Načíst další komentáře</button>';
  						
  						echo '<script>var article = '.$article_id.'</script>';
  					} 
  					else echo '<p>Zatím zde nejsou žádné Komentáře, tak nějaký přidej</p>';
  				?>
  			</div>
  		</div>
  	</div>
  </body>
</html>
