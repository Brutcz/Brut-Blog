<html>
  <head>
    <?php include('head.php'); ?>
  <title>Brut Blog - Stránka nenalezena</title>
  </head>
  <body>
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1>404 - Tuto stránku prostě někdo ukradl</h1>
				<p>Tato stránka nebyla nalezena, tak prosím pokračuj na hlavní stránku nebo to zkuste později...Děkujeme za pochopení</p>
				<i class="fa fa-frown-o" style="font-size:20em;text-align:center;width:100%;"></i>
				<a href="./" class="btn btn-lg btn-block btn-info"><i class="fa fa-reply fa-fw"></i> Zpět na hlavní stránku</a>
  			</div>
  		</div>
  	</div>
  </body>
</html>
