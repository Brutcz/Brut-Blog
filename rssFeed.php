<!DOCTYPE html>
<html>
  <head>
    <title>Brut Blog - Novinky</title>
    <?php include('head.php'); ?>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1>RSS feedy</h1>
			    <div class="list-group">
	  			<?php
	  				$xml=simplexml_load_file("http://www.novinky.cz/rss/");
	  				$index=1;

	  				$image=$xml->image;
	  				echo '<a href="'.$image->link.'"><img src="'.$image->url.'" alt="'.$image->title.'"></a>';

	  				foreach ($xml->item as $item)
	  				{
	  					if($index > 10) break;
	  					$index++;

	  					$link=$item->link;
	  					$title=$item->title;
	  					$description=$item->description;
	  				
	  					?>
	  						<a href="<?php echo $link;?>" class="list-group-item" target="_blank">
	  							<h4 class="list-group-item-heading"><?php echo $title;?></h4>
	  							<p class="list-group-item-text"><?php echo $description;?></p>
	  						</a>
	  					<?php
	  				}
	  			?>
	  			</div>
	  			<h2>Výpis lokálních novinek</h2>
	  			<div class="list-group">
	  				<?php
	  					$xml=simplexml_load_file("http://88.81.76.204/rss"); //simplexml_load_file("./rss.php");
	  					$index=1;
	  					echo($xml);
	  					foreach($xml->item AS $item)
	  					{
	  						//if($index>10) break;
	  						$index++;
	  						$link=$item->link;
	  						$title=$item->title;
	  						$description=$item->description->p;
	  						$date=$item->date;
	  						$author=$item->author;
	  						?>
		  						<a href="<?php echo $link;?>" class="list-group-item">
		  							<h4 class="list-group-item-heading"><?php echo $title;?></h4>
		  							<p class="list-group-item-text"><?php echo $description;?></p>
		  							<p class="list-group-item-text"><?php echo "Napsal/a: ".$author." v ".$date;?></p>
		  						</a>
	  						<?php
	  					}
	  				?>
	  			</div>
  			</div>
  		</div>
  	</div>
  </body>
</html>
