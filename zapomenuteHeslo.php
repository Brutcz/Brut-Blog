<!DOCTYPE html>
<html>
  <head>
    <title>Brut Blog - obnovení zapomenutého hesla</title>
    <?php include('head.php'); ?>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1>Obnovení hesla</h1>
			    <form method="post" action="./tools/forgetPass.php" class="center-block" style="width:50%;">
			    	<fieldset>
			    		<div class="input-group">
			              	<span class="input-group-addon"><i class="fa fa-user"></i></span>
			              	<input name="name" type="text" class="form-control" placeholder="Uživatelské jméno" rel="popover" data-container="body" data-placement="right" data-content="Napiš své přihlašovací jméno">
			            </div>
			            <div class="input-group">
			              	<span class="input-group-addon"><i class="fa fa-at"></i></span>
			            	<input name="email" type="email" class="form-control" placeholder="email" rel="popover" data-container="body" data-placement="right" data-content="Napiš svůj email">
			            </div>
			            <p><button id="getPass" class="btn btn-primary center-block" style="margin-top:10px;">Získat nové heslo</button></p>
			    	</fieldset>
			    </form>
  			</div>
  		</div>
  	</div>
  </body>
</html>
