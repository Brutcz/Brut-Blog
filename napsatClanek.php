<!DOCTYPE html>
<html>
  <head>
    <title>Brut Blog</title>
    <?php include('head.php'); ?>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			<?php
				if(!haveUserPerm($_SESSION['login']['nick'],'article_write'))
				{
					$_SESSION['msg']['error_type'][1]="Na tuto akci nemáte oprávnění";
					$_SESSION['msg']['error_type'][0]=1;
					header("Location: /");
				}
			?>
			    <h1>Napsat článek</h1>
			    <form method="post" id="newsForm" action="./tools/writeArticle.php" style="margin-top:15px;">
			    	<fieldset>
			            <div class="input-group">
			                <input name="title" type="text" class="form-control" placeholder="Titulek" rel="popover" data-container="body" data-placement="right" data-content="Napiš titulek článku">
			            </div>
			            <div class="input-group">
							<select class="form-control" name="articleCategory">
				            <?php
				            	$result=$mysqli->query("SELECT * FROM `category`");
				               	while ($row=$result->fetch_array())
				               	{
				               		echo '<option value="'.$row['id_category'].'">'.$row['name'].'</option>';
				               	}
				            ?>
							</select>
						<div class="btn-group" data-toggle="buttons" style="margin-bottom:5px;margin-top:10px;">
							<label for="type_a" class="btn btn-primary active"><input type="radio" name="type" value="article" id="type_a" checked><i class="fa fa-pencil"></i>&nbsp; Článek &nbsp;</label>
							<label for="type_v" class="btn btn-primary"><input type="radio" name="type" value="video" id="type_v"><i class="fa fa-video-camera"></i>&nbsp; Video &nbsp;</label>
						</div>
			            </div>
			            <div style="margin-top:10px;margin-bottom:10px;">
			            	<input type="checkbox" name="onlyFor" id="onlyFor" value="1"> Jen pro registrované
			            </div>
			            <script type="text/javascript" src="./js/tinymce/tinymce.min.js"></script>
						<script type="text/javascript">
						tinymce.init({
						    selector: "textarea",
						    theme: "modern",
						    plugins: [
						        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
						        "searchreplace wordcount visualblocks visualchars code fullscreen",
						        "insertdatetime media nonbreaking save table contextmenu directionality",
						        "emoticons template paste textcolor colorpicker textpattern imagetools"
						    ],
						    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
						    toolbar2: "preview media | forecolor backcolor emoticons",
						    image_advtab: true,
						    templates: [
						        {title: 'Test template 1', content: 'Test 1'},
						        {title: 'Test template 2', content: 'Test 2'}
						    ]
						});
						</script>
			            <textarea id="postNews" name="post" class="form-control" placeholder="Text článku..."></textarea>
			            <p><button id="postNewsBtn" class="btn btn-primary" style="margin-top:10px;">Napsat příspěvek</button></p>
		            </fieldset>    
	            </form>
  			</div>
  		</div>
  	</div>
  </body>
</html>
