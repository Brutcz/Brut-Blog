<html>
  <head>
    <?php include('head.php'); ?>
<?php
	$query=trim($_GET['query']);
  

  $title=$query;
?>
  <title>Brut Blog - Hledej</title>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1><?php echo "Hledej: ".$title; ?></h1>
          <h2>Články a videa</h2>
			   	<?php
            $query = "%".$query."%";

            $stmt=$mysqli->prepare(
            "SELECT A.id_article, A.title, A.article, C.name, M.username, A.type, DATE(date) AS date, A.for_registred, A.comment_count
              FROM article A
              LEFT JOIN category C ON (C.id_category=A.category) 
              LEFT JOIN members M ON(M.id=A.author)
              WHERE A.title LIKE (?) OR A.article LIKE (?)
             ");
            $stmt->bind_param("ss",$query,$query);
            $stmt->execute();
            $stmt->bind_result($id_article, $title,$article,$category,$author,$type,$date,$onlyFor,$comment_count);
            $stmt->store_result();
            if($stmt->num_rows<1) echo '<p>Nic nebylo nalezeno</p>';
              else echo "<p>Počet nalezených článků:".$stmt->num_rows."</p>";
            while($stmt->fetch())
            {
                $article=html_entity_decode($article);

                if($type=="article")
                {
                  if(strlen($article)>70) $article="<p>".substr(trim(getTextBetweenTags($article,"p")),0,70)."...</p>";
                    else $article="<p>".trim(getTextBetweenTags($article,"p"))."</p>";
                }
                ?>
                <div id="<?php echo $id_article;?>" class="panel panel-primary">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                      <?php 
                        echo $title;

                      ?>
                    </h3>
                    <?php
                      
                        if(isset($_SESSION['login'])&&haveUserPerm($_SESSION['login']['nick'],'article_change'))
                        {
                          echo '<i class="fa fa-2x fa-times-circle-o delete_article"></i>';
                        }
                    ?>
                  </div>
                  <div class="panel-body">
                    <?php if(($onlyFor==1)&&isset($_SESSION['login'])&&(isUserLogIn($_SESSION['login']['ID'],$_SESSION['login']['sessionCode'])))
                      {
                        echo $article;
                      } elseif(($onlyFor==1)&&(!isset($_SESSION['login']))){
                          echo '<div class="alert alert-info" role="alert">Pro čtení tohoto článku musíte být přihlášeni!</div>';
                      } else {
                        echo $article;
                      }
                    ?>
                    <br><br>
                    <a href="./clanek/<?php echo $id_article;?>" class="btn btn-success">Číst dále</a>
                  </div>
                  <div class="panel-footer">
                    <?php echo $date.' Autor: <a href="./uzivatel/'.$author.'">'.$author.'</a>';?>&nbsp;&nbsp;
                    <a href="./hledej/category%3D<?php echo $category;?>">
                      <span class="label label-warning"><?php echo $category;?></span>
                    </a>
                    Počet komentářů: <?php echo $comment_count;?>
                    <?php
                      if(isset($_SESSION['login'])&&haveUserPerm($_SESSION['login']['nick'],'article_change'))
                      {
                        echo '<i class="fa fa-2x fa-pencil-square-o edit_article"></i>';
                      }
                    ?>
                  </div>
              </div>
              <div class="list-group">
            <?php
            }
            $stmt->close();

            $stmt=$mysqli->prepare("SELECT username, email, sex FROM members WHERE username LIKE (?) OR email LIKE (?)");
            $stmt->bind_param("ss",$query,$query);
            $stmt->execute();
            $stmt->bind_result($username,$email,$sex);
            if(($stmt->num_rows)<1) echo '<p>Nebyli nalezeni žádní uživatelé</p>';
            while($stmt->fetch())
            {
              ?>
                <a href="./uzivatel/<?php echo $username?>" class="list-group-item">
                  Jméno: <?php echo $username?><br>Email: <?php echo $email?><br>
                  <div class="btn-group" data-toggle="buttons" style="margin-top:10px;">
                    <?php
                      if(strcmp($sex,"F")==0){
                    ?>
                    <button type="button" class="btn btn-primary active">&nbsp; Žena &nbsp;</button>
                    <?php
                      } else{
                    ?>
                    <button type="button" class="btn btn-primary active">&nbsp; Muž &nbsp;</button>
                    <?php
                      }
                    ?>
                  </div>
                </a>
              <?php
            }
            $stmt->close();
          ?>
          </div>
  			</div>
  		</div>
  	</div>
  </body>
</html>
