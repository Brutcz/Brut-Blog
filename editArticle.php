<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  	<div class="modal-dialog" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
        		<h4 class="modal-title" id="myModalLabel"></h4>
      		</div>
      		<div class="modal-body">
		      	<form method="post" id="newsEditForm" action="" onsubmit="updateArticle(); return false;" style="margin-top:15px;">
		            <div class="input-group">
		                <input name="title" type="text" class="form-control" style="margin-bottom:5px;" placeholder="Titulek" rel="popover" data-container="body" data-placement="right" data-content="Napiš titulek článku">
		            </div>
		            <div style="margin-bottom:10px;">
		            	<input type="checkbox" name="onlyFor" id="onlyFor" value="1"> Jen pro registrované
		            </div>
		            <div style="margin-bottom:10px;">
		            	<input type="hidden" name="id_article" id="id_article">
		            </div>
		            <script type="text/javascript" src="./js/tinymce/tinymce.min.js"></script>
					<script type="text/javascript">
					tinymce.init({
		    		selector: "textarea",
		    		theme: "modern",
		    		plugins: [
				        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
				        "searchreplace wordcount visualblocks visualchars code fullscreen",
				        "insertdatetime media nonbreaking save table contextmenu directionality",
				        "emoticons template paste textcolor colorpicker textpattern imagetools"
				    ],
				    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
				    toolbar2: "preview media | forecolor backcolor emoticons",
				    image_advtab: true,
				    templates: [
				        {title: 'Test template 1', content: 'Test 1'},
				        {title: 'Test template 2', content: 'Test 2'}
				    ]
				});
				</script>
		            <textarea id="postNews" name="post" class="form-control" placeholder="Text článku..."></textarea>
		      	</div>
		      	<div class="modal-footer">
		        	<button type="button" class="btn btn-default" data-dismiss="modal">Zavřít</button>
		        	<button type="submit" class="btn btn-primary" id="newsSave">Uložit změny</button>
		      	</div>
		    </form>
    	</div>
  	</div>
</div>