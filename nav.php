<script src="./js/nav.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="true">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="/">Brut Blog</a>
	</div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    <ul class="nav navbar-nav">
	        <li class="active"><a href="/">Novinky<span class="sr-only">(current)</span></a></li>
	        <li><a href="./prispevky">Přispěvky</a></li>
	        <li><a href="./videa">Videa</a></li>
	        <li class="dropdown">
	          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Programování&nbsp;<i class="fa fa-hand-o-down"></i></a>
	          <ul class="dropdown-menu">
	          	<?php
	          		$result=$mysqli->query("SELECT * FROM `menu`");
	          		while($row=$result->fetch_array())
	          		{
	          			echo '<li><a href="'.$row['href'].'">'.$row['title'].'</a></li>';
	          		}
	          	?>
	            <li role="separator" class="divider"></li>
	            <li><a href="./ascii">ASCII</a></li>
	            <li role="separator" class="divider"></li>
	            <li><a href="#shitCode">Nejlepší kódy</a></li>
	          </ul>
		    </li>
		    <li><a href="./rssFeed">RSS feedy</a></li>
	    </ul>
	    <form method="get" action="" class="navbar-form navbar-left" role="search" onsubmit="search();return false;">
	        <div class="form-group">
	          <input id="searchQuery" type="text" class="form-control" placeholder="Hledej...">
	        </div>
	        <button type="submit" class="btn btn-default">Hledat&nbsp;&nbsp;<i class="fa fa-fw fa-search"></i></button>
	    </form>
	    <ul class="nav navbar-nav navbar-right">
	    	<?php
	    		if(!isset($_SESSION['login']['nick']))
	    		{
	    			?>
				    <li><a href="./prihlaseni">Přihlášení/Registrace</a></li>
	    			<?php
	    		} else {
	    			?>
	    			<li class="dropdown">
			        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Prihlášen jako <b><?php echo $_SESSION['login']['nick']; ?></b>&nbsp;<i class="fa fa-hand-o-down"></i></a>
			          	<ul class="dropdown-menu">
			          	<?php
			          		if(haveUserPerm($_SESSION['login']['nick'],'article_write'))
			          		{
			          			echo '<li><a href="./napsatClanek"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Napsat článek</a></li>';
			          		}

			            	//echo '<li><a href="#">Another action</a></li>';
			          		echo '<li role="separator" class="divider"></li>';	//Pokud uživatel nemá žádné další oprávnění - netisknout
			          	?>
			            	<li><a href="#settings"><i class="fa fa-cog"></i>&nbsp;&nbsp;Nastavení</a></li>
			            	<li role="separator" class="divider"></li>
			            	<li><a href="./logout"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Odhlásit se</a></li>
			          	</ul>
			        </li>
	    			<?php
	    		}
	    	?>
	        
	     </ul>
	    </div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>
<?php include ("errMsg.php"); ?>