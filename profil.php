<?php $name=$_GET['name'];?>
<!DOCTYPE html>
<html>
  <head>
    <title>Brut Blog - Profil <?php echo $name;?></title>
    <?php include('head.php'); ?>
  </head>
  <body>
	<?php include('nav.php'); ?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1>Profil uživatele <span style="font-style:italic;"><?php echo $name;?></span></h1>
			    <?php
			    	$stmt=$mysqli->prepare("SELECT id, email, sex FROM members WHERE username=?");
			    	$stmt->bind_param("s",$name);
			    	$stmt->execute();
			    	$stmt->bind_result($id,$email,$sex);
			    	$stmt->fetch();
			    	$stmt->close();
			    ?>
			    <div class="input-group" style="margin-bottom:5px;">
					<span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
					<input class="form-control" type="email" name="email" placeholder="Email" value="<?php echo $email ?>" readonly>
				</div>
				<div class="btn-group" data-toggle="buttons" style="margin-top:10px;">
					<?php
						if(strcmp($sex,"F")==0){
					?>
					<button type="button" class="btn btn-primary active">&nbsp; Žena &nbsp;</button>
					<?php
						} else{
					?>
					<button type="button" class="btn btn-primary active">&nbsp; Muž &nbsp;</button>
					<?php
						}
					?>
				</div>
				<hr>
				<p style="font-size:8pt;">*Pro přečtení zašedlých článku musíte být přihlášeni</p>
				<div id="articleList" class="list-group">
				<?php
				   	$stmt=$mysqli->prepare(
				   	"SELECT A.id_article, A.title, C.name, DATE(date) AS date, A.for_registred 
			    	FROM article A 
			    	LEFT JOIN category C ON (C.id_category=A.category)
			    	WHERE A.author=? 
			    	ORDER BY A.id_article DESC 
			    	LIMIT 20");
				   	$stmt->bind_param("s",$id);
				   	$stmt->execute();
				   	$stmt->bind_result($id_article,$title,$category,$date,$for_only);
				   	$stmt->store_result();
					$n = $stmt->num_rows;
				   	while($stmt->fetch())
				   	{
				   		$class=(($for_only==1)&&(!isset($_SESSION['login'])))?"list-group-item disabled":"list-group-item";
						echo '<a id="'.$id_article.'" class="'.$class.'" href="./clanek/'.$id_article.'">'.$title.'&nbsp;&nbsp;<span class="label label-warning">'.$category.'</span>&nbsp;&nbsp;'.$date.'</a>';    		
				   	}
				   	$stmt->close();
				   	if($n==20){
				   		echo '<script src="./js/profil.js"></script>';
				   		echo '<script>var UID="'.$id.'";</script>';
				   		echo '<button id="loadMore" class="btn btn-default center" data-loading-text="Načítám..." style="margin-top:15px;">Načíst další články od autora</button>';
				   	} 
			    ?>
				</div>
  			</div>
  		</div>
  	</div>
  </body>
</html>
