<!DOCTYPE html>
<html>
  <head>
    <title>Brut Blog - Novinky</title>
    <?php include('head.php'); ?>
  </head>
  <body>
	<?php include('nav.php'); 
		if(isset($_SESSION['login'])&&haveUserPerm($_SESSION['login']['nick'],'article_change'))
		{
			echo '<script src="./js/news.js"></script>';
		}
	?>  	
  	<div id="content">
  		<div class="container">
  			<div class="jumbotron">
			    <h1>Novinky</h1>
			    <?php
			    	$result=$mysqli->query(
			    		"SELECT A.id_article, A.title, A.article, A.type, C.name, M.username, DATE(date) AS date, A.for_registred, A.comment_count 
			    		FROM article A 
			    		LEFT JOIN category C ON (C.id_category=A.category) 
			    		LEFT JOIN members M ON(M.id=A.author)
			    		ORDER BY A.id_article DESC 
			    		LIMIT 20
			    		");
			    	while($row=$result->fetch_array())
			    	{
			    		$row['title']=html_entity_decode($row['title']);
			    		$row['article']=html_entity_decode($row['article'],ENT_HTML5,"utf-8");
			    		if($row['type']=="article")
			    		{
			    			if(strlen($row['article'])>70) $row['article']="<p>".substr(trim(getTextBetweenTags($row['article'],"p")),0,70)."...</p>";
			    				else $row['article']="<p>".trim(getTextBetweenTags($row['article'],"p"))."</p>";
			    		}
			    ?>
			    <div id="<?php echo $row['id_article'];?>" class="panel panel-primary">
				  	<div class="panel-heading">
				    	<h3 class="panel-title">
				    		<?php 
				    			echo $row['title'];

				    		?>
				    	</h3>
				    	<?php
				    		
				    			if(isset($_SESSION['login'])&&haveUserPerm($_SESSION['login']['nick'],'article_change'))
				    			{
				    				echo '<i class="fa fa-2x fa-times-circle-o delete_article"></i>';
				    			}
				    	?>
				  	</div>
				  	<div class="panel-body">
				    	<?php if(($row['for_registred']==1)&&isset($_SESSION['login'])&&(isUserLogIn($_SESSION['login']['ID'],$_SESSION['login']['sessionCode'])))
				    		{
				    			echo $row['article'];
				    		} elseif(($row['for_registred']==1)&&(!isset($_SESSION['login']))){
				    				echo '<div class="alert alert-info" role="alert">Pro čtení tohoto článku musíte být přihlášeni!</div>';
				    		} else {
				    			echo $row['article'];
				    		}
				    	?>
				    	<br><br>
				    	<a href="./clanek/<?php echo $row['id_article'];?>" class="btn btn-success">Číst dále</a>
				  	</div>
				  	<div class="panel-footer">
				  		<?php echo $row['date'].' Autor: <a href="./uzivatel/'.$row['username'].'">'.$row['username'].'</a>';?>&nbsp;&nbsp;
				  		<a href="./hledej/category%3D<?php echo $row['name'];?>">
				  			<span class="label label-warning"><?php echo $row['name'];?></span>
				  		</a>
				  		Počet komentářů: <?php echo $row['comment_count'];?>
				  		<?php
				  			if(isset($_SESSION['login'])&&haveUserPerm($_SESSION['login']['nick'],'article_change'))
				    		{
				    			echo '<i class="fa fa-2x fa-pencil-square-o edit_article"></i>';
				    		}
				  		?>
				  	</div>
				</div>
				<?php
					}
				?>
  			</div>
  		</div>
  	</div>
  	<?php
  		if(isset($_SESSION['login'])&&haveUserPerm($_SESSION['login']['nick'],'article_change'))
		{
  			include('editArticle.php');	
  		} ?>
  </body>
</html>
