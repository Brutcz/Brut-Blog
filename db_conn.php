<?php
	$host="localhost"; // Host name 
	$db_username="web"; // Mysql username 
	$db_password="jw4EXwsMACpnscXW"; // Mysql password 
	$db_name="blog"; // Database name 

	// Connect to server and select databse.
	$mysqli = new mysqli($host, $db_username, $db_password, $db_name);
	if ($mysqli->connect_errno) {
    	echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	//echo $mysqli->host_info . "\n";  //Kontrola připojení

	$mysqli->query("SET CHARACTER SET utf8");

	session_start();

	function isUserLogIn($uID,$hash) 
	{
		$mysqli= $GLOBALS['mysqli'];
		$stmt = $mysqli->prepare("SELECT `userID` FROM `uaccess` WHERE `userID` =? AND `randomCode` =?");
		$stmt->bind_param("is",$uID,$hash);
		$stmt->execute();
		$stmt->store_result();
		$n = $stmt->num_rows;
		$stmt->close();

		if($n == 1) {
			return(true);
		} else {
			return(false);
		}
	}

	function getUserName($uID)
	{
		$mysqli= $GLOBALS['mysqli'];
		$stmt=$mysqli->prepare("SELECT `username` FROM `members` WHERE `id`=?");
		$stmt->bind_param("i",$uID);
		$stmt->execute();
		$stmt->bind_result($name);
		$stmt->fetch();
		$stmt->close();

		return ($name);
	}

	function getUserID($name)
	{
		$mysqli= $GLOBALS['mysqli'];
		$stmt=$mysqli->prepare("SELECT `id` FROM `members` WHERE `username`=?");
		$stmt->bind_param("s",$name);
		$stmt->execute();
		$stmt->bind_result($uID);
		$stmt->fetch();
		$stmt->close();

		if(!empty($uID)) return $uID;
			else return false;
	}

	function haveUserPerm($user,$perm)
	{
		$mysqli= $GLOBALS['mysqli'];
		$stmt=$mysqli->prepare(
		"SELECT M.id,M.username,R.name,RP.id_perm
		FROM role_perm RP 
		LEFT JOIN role R ON (RP.id_role = R.id_role)
		LEFT JOIN user_role UR ON (R.id_role = UR.id_role)
		LEFT JOIN members M ON (UR.id_user = M.id)
		LEFT JOIN permissions P ON (RP.id_perm = P.id_perm)
		WHERE (P.perm_name =?) AND (M.username =?)
		");
		$stmt->bind_param("ss",$perm,$user);
		$stmt->execute();
		$stmt->store_result();
		$n = $stmt->num_rows;
		$stmt->close();
		if($n==1)
		{
			return true;
		} else return false;
	}

	function getTextBetweenTags($string, $tagname)
	{
		$pattern = "/<$tagname>(.*?)<\/$tagname>/";
		if(preg_match($pattern, $string, $matches)) return $matches[1];
			else return $string;
	}
?>